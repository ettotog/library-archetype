# Library Archetype

```xml
<project>
    <repositories>
        <repository>
            <id>ettotog</id>
            <url>https://gitlab.com/api/v4/groups/io.gitlab.ettotog/-/packages/maven</url>
        </repository>
    </repositories>
    <pluginRepositories>
        <pluginRepository>
            <id>ettotog</id>
            <url>https://gitlab.com/api/v4/groups/io.gitlab.ettotog/-/packages/maven</url>
        </pluginRepository>
    </pluginRepositories>
</project>
```

```sh
mvn archetype:generate \
  -DarchetypeGroupId=io.gitlab.ettotog.archetype \
  -DarchetypeArtifactId=library \
  -DarchetypeVersion=0.0-SNAPSHOT \
  -DgroupId=com.example \
  -DartifactId=example-project
```